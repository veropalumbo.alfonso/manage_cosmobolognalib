#!/usr/bin/env python

from setuptools import setup

setup(name='Manage_CosmoBolognaLib',
      version='0.0.6',
      description="Manage installation of CosmoBolognaLib from gitlab repository",
      author='Alfonso Veropalumbo',
      author_email='alfonso.veropalumbo@uniroma3.it',
      packages=['Manage_CosmoBolognaLib',\
                'Manage_CosmoBolognaLib.utilities',\
                'Manage_CosmoBolognaLib.gitcontrol',\
                'Manage_CosmoBolognaLib.managecbl']
     )
