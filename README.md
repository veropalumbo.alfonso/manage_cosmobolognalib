# Manage_CosmoBolognaLib

This package helps the user in the installation of the CosmoBolognaLib in a Google Colab environment.

A Google account is required (?); optionally also a working Google Drive.

# Quick guide

First you should initialize the environment.
The constructor install (if necessary) all the required dependencies.
using python-apt package.
Also, the repository is initialized and the PYTHONPATH updated.

“getCBL = GetCosmoBolognaLib()“

The call operator search for an existing installation of the CosmoBolognaLib (in form of .tar.gz)
at path "root_dir+CBL_file+.tar.gz"

If the file "root_dir+CBL_file+.tar.gz" exists, it is copied in the /content/CosmoBolognaLib folder and extracted
If the file "root_dir+CBL_file+.tar.gz" doesn't exists, the CosmoBolognaLib repository is cloned and the libraries
compiled. The results is then compressed and sent to root_dir.

"getCBL(CBL_file="CosmoBolognaLib", root_dir="/content/drive/My Drive", branch="develop")"

# Step-by-step installation

This command clones and compiles CosmoBolognaLib in the root of the Google Colaboratory virtual machine ("/content/").
Notice that you will lose all the changes at the end of the colab session

getCBL.clone(branch=branch_name) 

getCBL.compile()

You can then move to your Google Drive

getCBL.self.send_tar_to_drive(CBL_file, root_dir)

# Updating existing installation

After you imported your libraries, you may wish to check for updates. 
You can use:

getCBL.pull()

And then compile and store the file
getCBL.compile(CBL_file, root_dir)

getCBL.self.send_tar_to_drive(CBL_file, root_dir)

# Clean working directory

To wipe all installation in your /content folder

getCBL.reset()

# WARNING

If you make changes to the libraries AFTER the import command
you should reload your kernel in order to see this changes.

# Upload to pip

1) Change version in setup.py
2) Build with python3 setup.py sdist bdist_wheel
3) Upload using python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/* 
