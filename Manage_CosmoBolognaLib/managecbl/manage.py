'''
Created on 23 mar 2020

@author: alfonso
'''

import os
import sys
import apt
import tarfile
from shutil import copyfile
from shutil import move, rmtree

from Manage_CosmoBolognaLib.utilities import run_command
from Manage_CosmoBolognaLib.gitcontrol import GitManager

class ManageCosmoBolognaLib:
    '''
    
    '''
    
    '''
    List of dependencies
    '''
    apt_dependencies = ["libgfortran5",\
                        "libgsl-dev", "libgsl27", "libgslcblas0",\
                        "libfftw3-dev",\
                        "libboost-all-dev",\
                        "libcfitsio9", "libcfitsio-dev",\
                        "swig"]

    def __init__(self):
        '''
        
        Constructor of ManageCosmoBolognaLib
        '''
    
        self.install_dependencies()
        self.append_to_path()
        self.git_repo = GitManager(destination="/content/CosmoBolognaLib/", repo_url="https://gitlab.com/federicomarulli/CosmoBolognaLib.git")

    def reset(self):
        '''
        
        Wipe all local installation files
        '''
        os.chdir("/content/")
        run_command("rm -rf /content/CosmoBolognaLib*")

    def __call__(self, CBL_file=None, root_dir="/content/drive/My Drive", branch="develop"):
        '''
        
        
        :param CBL_file:
        :param root_dir:
        :param branch:
        '''
    
        if CBL_file is None:
            print("No CBL file provided!")
        else:
            if self.check_file(root_dir+"/"+CBL_file+".tar.gz"):
                print("CBL file found %s"%(root_dir+"/"+CBL_file+".tar.gz"))
                self.extract(CBL_file, root_dir)
            else:
                print("No CBL file found %s"%(root_dir+"/"+CBL_file+".tar.gz"))
                self.clone(branch)      
                self.compile()
                self.send_tar_to_drive(CBL_file, root_dir)
  
    def clone(self, branch="develop"):
        '''
        
        Clone the CosmoBolognaLib repository and
        checkout the required branch
        
        :param branch: branch name
        '''

        self.git_repo.clone()

        self.git_repo.change_branch(branch)

    def pull(self):
        '''
        
        Pull current branch
        '''

        self.git_repo.pull()

    def compile(self, CBL_file=None, root_dir="/content/drive/My Drive"):
        '''
        
        Compile the CosmoBolognaLib.
        This runs 'make' and 'make python' commands
        Optionally stores the results in the user's Google Drive
        
        :param CBL_file: CosmoBolognaLib package file name without extension
        :param root_dir: Location of the package
        '''

        curdir = os.getcwd()
        os.chdir("/content/CosmoBolognaLib/")

        #run_command("make")
        run_command("make python")
        os.chdir(curdir)

        if CBL_file is not None:
            self.send_tar_to_drive(CBL_file, root_dir)

    def send_tar_to_drive(self, CBL_file, root_dir="/content/drive/My Drive"):
        '''
        
        Send the package to the user's Google Drive
        
        :param CBL_file: CosmoBolognaLib package file name without extension
        :param root_dir: Location of the package
        '''

        CBLtar = tarfile.open("/content/CosmoBolognaLib.tar.gz", mode="w:gz")
        CBLtar.add("/content/CosmoBolognaLib/", arcname="")
        CBLtar.close()
        move("/content/CosmoBolognaLib.tar.gz", root_dir+"/"+CBL_file+".tar.gz")

    def extract(self, CBL_file, root_dir="/content/drive/My Drive"):
        '''
        Extract the CosmoBolognaLib file 
        
        :param CBL_file: CosmoBolognaLib package file name without extension
        :param root_dir: Location of the package
        '''
    
        copyfile(root_dir+"/"+CBL_file+".tar.gz", "/content/CosmoBolognaLib.tar.gz")
        CBLtar = tarfile.open("/content/CosmoBolognaLib.tar.gz", mode="r:gz")
        CBLtar.extractall("/content/CosmoBolognaLib/")
        CBLtar.close()
        os.remove("/content/CosmoBolognaLib.tar.gz")
        
    def append_to_path(self):
        '''
        Append the installation directory in the python path.
        
        '''

        sys.path.insert(1, "/content/CosmoBolognaLib/Python/")
        msg = "CosmoBolognaLib succesfully added to the PYTHONPATH!\n"
        msg += "You can now use the CosmoBolognaLib using the import command."
        print(msg)

    def check_file(self, CBL_tar):
        '''
        Check that the tar file containing the compiled
        CosmoBolognaLib exists.
        
        :param CBL_tar:
        '''

        if not os.path.isfile(CBL_tar):
            msg = "%s can't be found.\n"%CBL_tar
            msg += "You should specify a correct path for the CosmoBolognaLib installation \n"
            msg += "or you should install it first by running self.compile()"
            return False
        else:
            return True

    def install_apt_package(self, pkg_name, cache):
        '''
        This function install a package from apt repository
        This is used internally
        :param pkg_name: name of the package
        :param cache: apt cache
        '''

        pkg = cache[pkg_name]
        if pkg.is_installed:
            print("{pkg_name} already installed".format(pkg_name=pkg_name))
        else:
            pkg.mark_install()
            try:
                cache.commit()
                cache.update()
            except Exception as arg:
                print("Sorry, package installation failed [{err}]".format(err=str(arg), file=sys.stderr))
            else:
                print("{pkg_name} successfully installed".format(pkg_name=pkg_name))
  
    def install_dependencies(self):
        '''
        This function install all the required dependencies.
        '''
        cache = apt.cache.Cache()
        cache.update()
        cache.open()
        [self.install_apt_package(pkg, cache) for pkg in self.apt_dependencies]
