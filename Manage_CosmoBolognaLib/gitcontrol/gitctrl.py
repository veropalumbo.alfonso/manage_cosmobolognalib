'''
Created on 23 mar 2020

@author: alfonso
'''

from Manage_CosmoBolognaLib.utilities import run_command

import os

class GitManager:
    '''
    
    class to manage generic git repository
    '''
    def __init__(self, destination, repo_url=""):
        '''
        Initialize the git manager
        
        :param destination: local folder to store the repository
        :param repo_url: url of the repository
        '''
        
        self.destination = destination
        self.repo_url = repo_url
  
    def clone(self):
        '''
        
        Clone the repository from self.repo_url
        to self.destination
        '''
        run_command("git clone %s %s"%(self.repo_url, self.destination))
  
    def change_branch(self, branch):
        '''
        Checkout to a new branch
        :param branch: name of the branch. 
        '''
        curdir = os.getcwd()
        os.chdir(self.destination)
        run_command("git branch %s origin/%s"%(branch, branch))
        run_command("git checkout %s"%branch)
        os.chdir(curdir)

    def pull(self):
        '''
        Pull from current branch
        '''
        
        curdir = os.getcwd()
        os.chdir(self.destination)
        run_command("git pull")
        os.chdir(curdir)
