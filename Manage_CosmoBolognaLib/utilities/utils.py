'''
Created on 23 mar 2020

@author: alfonso
'''

import sys
import subprocess

def run_command(command):
    '''
    
    Execute a command via subprocess.Popen and 
    gets the output in real time.
    
    :param command: string containing the shell command
                    to be executed
    '''
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,\
                               shell=True,  encoding='utf8')
    while True:

        if process.stderr != None:
            err = process.stderr.readline()
            sys.stdout.write(err)
            sys.stdout.flush()
            break

        out = process.stdout.readline()
        
        if out != '':
            sys.stdout.write(out)
            sys.stdout.flush()

        elif process.poll() != None:
            break
